<?php
const IMPORT_DIRECTORY = __DIR__ . '/../imports/';
const EXPORT_DIRECTORY = __DIR__ . '/../exports/';

const DUMP_DATE       = '2022-06-29';
const FILTER_BOOKINGS = false;

const BRAND_ID   = 564;
const BRAND_NAME = 'LHH';

const FINANCE_CHECK_LIST      = [1, 2, 3, 4, 5, 6, 7, 8];

const FINANCIAL_YEAR_START_DATE = '2021-10-01';
const BOOKINGS_END_DATE         = DUMP_DATE;

const KNOWN_BOOKINGS_FILE = '../known.csv';
const ERROR_CHECK_FILE    = '../error_check.csv';

const NRL_PROPERTIES  = [
    478,
    508,
    510,
    538,
];

const HEADERS_FILTERED_BOOKINGS = [
    'Booking Ref',
    'Total Cost',
    'Total Rent',
    'Booking Fee',
    'Admin Fee',
    'Extras',
    'Due to owner RENT',
    'Due to owner EXTRAS',
    'Total Due to owner',
    'Paid to owner',
    'OWNER LIABILITY',
    'Total to LHH',
    'Comm Gross',
    'Comm Net',
    'Comm VAT',
    'Extras Comm Gross',
    'Extras Comm Net',
    'Extras Comm VAT',
    'Total Received From Customer',
    'Total Relating to Holiday',
    'Balance Due on Holiday',
    'SD Amount',
    'SD Paid',
    'SD Refunded',
    'Balance of SD Held',
    'Status',
    'Property ref',
    'Owner ref',
    'Holiday booked date',
    'Holiday start date',
    'Holiday end date',
    'Source',
    'Comm%',
    'Property Comm%',
    'Property Name',
    'Property Unique Ref',
    'Discount',
    'Special Offer',
    'Amount Due to Owner (today)',
    'Amount Due to LHH (today)',
    'Cancelled Date',
    'Cancellation Rate',
    'Booking Type',
    'Booking Fee Dup',
    'Reclaimed From Owner',
    'Paid Per Statement',
    'Email Pending',
    'Ready For Accounts',
    'Needs Confirmation',
    'Required Action',
    'Customer ID',
    'Discount Type',
    'Discount LHH',
    'Discount Owner',
    'Cash Received Allocated to Agency First',
    'Balance of Cash After Allocation to Agency',
    'Total due to the Home Owner',
    'Balance Remaining to Home Owner After Payments to Date',
    'Debtors Control Account',
    'Debtors2',
    'Bank Current Account',
    'Sykes Income',
    'Sykes Deferred',
    'Owner Deposit',
    'Owner Deferred',
];

const FINANCE_CHECK_HEADERS = [
    'Check 1',
    'Check 2',
    'Check 3',
    'Check 4',
    'Check 5',
    'Check 6',
    'Check 7',
    'Check 8',
    'Check 9',
    'Check 10',
    'Check 11',
    'Check 12',
    'Check 13',
    'Check 14',
    'Check 15',
    'Check 16',
];

const FINANCE_CHECK_HEADER_DESCRIPTIONS = [
    'Check 1'  => 'Total Cost - Total Rent - Booking Fee - Extras - Admin Fee',
    'Check 2'  => 'Total Cost - Total Due to owner - Total to LHH',
    'Check 3'  => 'Total Due to owner - Due to owner EXTRAS - Due to owner RENT',
    'Check 4'  => 'Total to LHH - Comm Gross - Booking Fee - Extras Comm Gross - Admin Fee',
    'Check 5'  => 'Total Rent - Due to owner RENT - (Comm Gross)',
    'Check 6'  => 'Extras - Due to owner EXTRAS - Extras Comm Gross - Admin Fee',
    'Check 7'  => 'Total Due to owner - Paid to owner - OWNER LIABILITY',
    'Check 8'  => 'Total Cost - Total Relating to Holiday - Balance Due on Holiday',
    'Check 9'  => 'Property Comm% - Comm%',
    'Check 10' => 'Paid to owner - Paid Per Statement',
    'Check 11' => 'Debtors Control Account - Bank Current Account - Sykes Income - Sykes Deferred - Owner Deposit - Owner Deferred',
    'Check 12' => 'Total Cost - Debtors Control Account - Bank Current Account',
    'Check 13' => 'Total Due to owner + Owner Deposit + Owner Deferred',
    'Check 14' => 'Balance Due on Holiday - Debtors Control Account',
    'Check 15' => 'Total Relating to Holiday + Sykes Income + Owner Deposit',
    'Check 16' => 'Debtors Control Account + Sykes Deferred + Owner Deferred',
];

const DODGY_BOOKINGS = [

];

