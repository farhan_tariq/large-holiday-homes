<?php

final  class PaymentStatus
{
    public const DEFERRED     = 0;
    public const RELEASED     = 1;
    public const VOID         = 2;
    public const REFUNDED     = 3;
    public const HELD         = 4;
    public const BANKED       = 5;
    public const DESTROYED    = 6;
    public const AUTHORISE    = 8;
    public const AUTHENTICATE = 9;
    public const CANCEL       = 10;
    public const ABORT        = 11;
    public const PAYMENT      = 12;
}
