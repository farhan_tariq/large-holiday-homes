<?php
require_once 'base.php';
require_once 'repository.php';

$bookings         = getAllBookings($conn);
$customerPayments = getCustomerPayments($conn, $bookings);

foreach ($bookings as $booking) {
    if (filterBooking($booking, FILTER_BOOKINGS)) {
        continue;
    }

    $booking->discount_share = getDiscountShare($booking);
    $booking->discount_type  = getDiscountType($booking);

    if ($booking->discount_type === 'Split Discount') {
        $booking->adjusted_rent -= ($booking->discount_share['lhh'] + $booking->discount_share['owner']);
    }

    $sd         = $booking->security_deposit_amount;
    $sdPaid     = getSecurityDepositPaidAmount($booking);
    $sdRefunded = 0;
    $offsets    = getSdRefundOffsets($booking);

    foreach ($offsets as $offset) {
        $sdRefunded += $booking->payments[$offset]->amount__number;
    }

    $sdRefunded += getSecurityDepositRefunded($booking);

    if ($sdPaid == 0 && $sd == $sdRefunded) {
        $booking->security_deposit_paid_amount = $sdPaid = $sdRefunded;
    }

    $totalReceivedFromCustomer = round($booking->total_received_from_customer - $booking->total_refunded_to_customer, 2);
    $totalRelatingToHoliday    = $totalReceivedFromCustomer;

    $ownerExtras = getOwnerExtrasTotal($booking);
    $extrasTotal = getExtrasCommGross($booking);
    $paidToOwner = 0; // No data available yet - need it from quickbooks

    $totalPrice      = $booking->rental_price + $booking->booking_fee + $extrasTotal + $ownerExtras + $booking->admin_fee;
    $totalDueToOwner = getTotalDueToOwner($booking);
    $ownerLiability  = round($totalDueToOwner - $paidToOwner, 2);

    // LHH booking data has missing security_deposit_paid amount if it has been refunded
    if ($totalReceivedFromCustomer > $totalPrice && $totalReceivedFromCustomer - $totalPrice == $sd && $sdPaid == 0) {
        $booking->security_deposit_paid_amount = $sdPaid = $sd;
    }

    $sdHeld = $sdPaid > 0 && $sdRefunded == 0 ? $sdPaid : round($sdPaid - $sdRefunded, 2);

    if ($totalReceivedFromCustomer > $totalPrice) {
        $totalRelatingToHoliday -= $sdHeld;
    }

    $balanceDueOnHoliday = round($totalPrice - $totalRelatingToHoliday, 2);
    $totalToLHH          = getTotalToLHH($booking);

    $cashReceivedAllocatedToAgencyFirst         = $booking->booking_fee + (($totalRelatingToHoliday - $booking->booking_fee) * ($booking->commission_rate));
    $balanceOfCashAfterAllocationLeftForOwner   = $totalRelatingToHoliday - $cashReceivedAllocatedToAgencyFirst;
    $balanceRemainingToOwnerAfterPaymentsToDate = $balanceOfCashAfterAllocationLeftForOwner - $totalDueToOwner;

    $debtorsControlAccount = $balanceDueOnHoliday;
    $debtors2              = 0;
    $bankCurrentAccount    = $totalRelatingToHoliday;
    $ownerDeposits         = round($balanceOfCashAfterAllocationLeftForOwner * -1, 2);
    $sykesIncome           = round(($bankCurrentAccount * -1) - $ownerDeposits, 2);
    $ownerDeferred         = round(($totalDueToOwner * -1) - $ownerDeposits, 2);
    $sykesDeferred         = round(($totalToLHH * -1) - $sykesIncome, 2);

    $row = [
        $booking->id,                                                              // 'Booking Ref'
        $totalPrice,                                                               // 'Total Cost'
        $booking->rental_price,                                                    // 'Total Rent'
        $booking->booking_fee,                                                     // 'Booking Fee'
        $booking->admin_fee,                                                       // 'Admin Fee'
        $extrasTotal + $ownerExtras,                                               // 'Extras'
        $totalDueToOwner - $ownerExtras,                                           // 'Due to owner RENT'
        $ownerExtras,                                                              // 'Due to owner EXTRAS'
        $totalDueToOwner,                                                          // 'Total Due to owner'
        $paidToOwner,                                                              // 'Paid to owner'
        $ownerLiability,                                                           // 'OWNER LIABILITY'
        $totalToLHH,                                                               // 'Total to LHH'
        getCommissionGross($booking),                                              // 'Comm Gross'
        getCommissionNet($booking),                                                // 'Comm Net'
        getCommissionVat($booking),                                                // 'Comm VAT'
        $extrasTotal,                                                              // 'Extras Comm Gross'
        getExtrasCommNet($booking),                                                // 'Extras Comm Net'
        getExtrasCommVAT($booking),                                                // 'Extras Comm VAT'
        $totalReceivedFromCustomer,                                                // 'Total Received From Customer'
        $totalRelatingToHoliday,                                                   // 'Total Relating to Holiday'
        $balanceDueOnHoliday,                                                      // 'Balance Due on Holiday'
        $booking->security_deposit_amount,                                         // 'SD Amount'
        $sdPaid,                                                                   // 'SD Paid'
        $sdRefunded,                                                               // 'SD Refunded'
        $sdHeld,                                                                   // 'Balance of SD Held'
        $booking->status,                                                          // 'Status'
        $booking->_fk_property,                                                    // 'Property ref'
        $booking->_fk_owner,                                                       // 'Owner ref'
        $booking->booked_date,                                                     // 'Holiday booked date'
        $booking->from_date,                                                       // 'Holiday start date'
        $booking->to_date,                                                         // 'Holiday end date'
        $booking->booking_created_by,                                              // 'Source'
        $booking->commission_rate,                                                 // 'Comm%'
        $booking->commission_rate,                                                 // 'Property Comm%'
        $booking->property_name,                                                   // 'Property Name'
        '',                                                                        // 'Property Unique Ref'
        $booking->discount,                                                        // 'Discount'
        $booking->special_offer,                                                   // 'Special Offer'
        $totalDueToOwner,                                                          // 'Amount Due to Owner (today)'
        '',                                                                        // 'Amount Due to LHH (today)'
        $booking->canceled,                                                        // 'Cancelled Date'
        '',                                                                        // 'Cancellation Rate'
        $booking->status == 6 ? 'Owner Booking' : 'Customer Booking',              // 'Booking Type'
        $booking->booking_fee,                                                     // 'Booking Fee Dup'
        '',                                                                        // 'Reclaimed From Owner'
        0,                                                                         // 'Paid Per Statement'
        $booking->email_pending,                                                   // 'Email Pending'
        $booking->ready_for_accounts,                                              // 'Ready for Accounts'
        $booking->needs_confirmation,                                              // 'Needs Confirmation'
        $booking->required_action,                                                 // 'Required Action'
        $booking->_fk_customer,                                                    // 'Customer ID'
        $booking->discount_type,                                                   // 'Discount Type'
        $booking->discount_share['lhh'],                                           // 'Discount LHH'
        $booking->discount_share['owner'],                                         // 'Discount Owner'
        $cashReceivedAllocatedToAgencyFirst,                                       // 'Cash Received Allocated to Agency First'
        $balanceOfCashAfterAllocationLeftForOwner,                                 // 'Balance of Cash After Allocation to Agency'
        $totalDueToOwner,                                                          // 'Total due to the Home Owner'
        $balanceRemainingToOwnerAfterPaymentsToDate,                               // 'Balance Remaining to Home Owner After Payments to Date'
        $debtorsControlAccount,                                                    // 'Debtors Control Account'
        $debtors2,                                                                 // 'Debtors2'
        $bankCurrentAccount,                                                       // 'Bank Current Account'
        $sykesIncome,                                                              // 'Sykes Income'
        $sykesDeferred,                                                            // 'Sykes Deferred'
        $ownerDeposits,                                                            // 'Owner Deposit'
        $ownerDeferred,                                                            // 'Owner Deferred'
    ];

    $row = array_combine(HEADERS_FILTERED_BOOKINGS, $row);

    $financeChecks->printBooking($argv, $booking, $row);
    $financeChecks->runFinanceChecks($argv, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    $financeChecks->sumFinanceChecks($row);
    $financeChecks->checkBooking($known, $booking, $row);

    $financeChecksResults = $financeChecks->getBookingsErrorCheck()[$booking->id];
    $financeChecksRow     = array_combine(FINANCE_CHECK_HEADERS, (array) $financeChecksResults);
    $row                  = array_merge($row, $financeChecksRow);

    if (empty($argv[1])) {
        fputcsv($out, $row);
    }
}

if (empty($argv[1])) {
    fclose($out);
    echo 'Saved file: ' . $outputFileName . PHP_EOL . PHP_EOL;
    $financeChecks->compareFinanceChecks();
    $financeChecks->appendDataToErrorCheck(ERROR_CHECK_FILE);
}

function getTotalDueToOwner($booking): float
{
    $vatMultiplier = in_array($booking->_fk_property, NRL_PROPERTIES) ? 1 : 1.2;
    $discount      = 0;

    $rentalPrice = ! empty($booking->discount_type) ? $booking->adjusted_rent - $booking->special_offer : $booking->rental_price;

    if ($booking->discount_type === 'Owner Discount') {
        $discount = $booking->discount_share['owner'];
    }

    return round($rentalPrice - ($rentalPrice * $booking->commission_rate * $vatMultiplier), 2) + $booking->owner_extras - $discount;
}

function getTotalToLHH($booking): float
{
    return round(($booking->booking_fee + getCommissionGross($booking) + getExtrasCommGross($booking)) + $booking->admin_fee, 2);
}

function getCommissionNet($booking): float
{
    $discountAmount = 0;
    $vatMultiplier  = in_array($booking->_fk_property, NRL_PROPERTIES) ? 1 : 1.2;
    $commission     = $booking->adjusted_rent * ($booking->commission_rate);
    $commission     -= $booking->special_offer * ($booking->commission_rate);

    if ($booking->discount_type === 'LHH Discount') {
        $discountAmount = $booking->discount_share['lhh'] / $vatMultiplier;
    }

    $commission -= $discountAmount;

    return round($commission, 2);
}

function getCommissionVAT($booking): float
{
    if (in_array($booking->_fk_property, NRL_PROPERTIES)) {
        return 0;
    }

    return round(getCommissionNet($booking) * 0.2, 2);
}

function getCommissionGross($booking): float
{
    return getCommissionNet($booking) + getCommissionVat($booking);
}

function getExtrasCommNet($booking): float
{
    $total = $booking->pet_cost + $booking->bed_cost + $booking->additional_cottage_cost;

    return round(($total * ($booking->commission_rate)), 2);
}

function getExtrasCommVAT($booking): float
{
    if (in_array($booking->_fk_property, NRL_PROPERTIES)) {
        return 0;
    }

    return round(getExtrasCommNet($booking) * 0.2, 2);
}

function getExtrasCommGross($booking): float
{
    return round(getExtrasCommNet($booking) + getExtrasCommVAT($booking), 2);
}

function getOwnerExtrasTotal($booking): float
{
    $vatMultiplier         = in_array($booking->_fk_property, NRL_PROPERTIES) ? 1 : 1.2;
    $total                 = $booking->pet_cost + $booking->bed_cost + $booking->additional_cottage_cost;
    $booking->owner_extras = $ownerExtras = round(($total - getExtrasCommNet($booking) * $vatMultiplier), 2);

    return $ownerExtras;
}

function getSecurityDepositRefunded($booking): float
{
    $refundAmount = 0;
    foreach ($booking->payments as $payment) {
        if ((int) $payment->payment_type === PaymentTypes::GOOD_HOUSE_KEEPING_DEPOSIT && in_array($payment->payment_status, [PaymentStatus::REFUNDED, PaymentStatus::CANCEL, PaymentStatus::DESTROYED])) {
            $refundAmount += $payment->amount__number;
        }
    }

    return $refundAmount;
}

function getSdRefundOffsets($booking): array
{
    $offsets = [];
    $ghbs    = array_filter($booking->payments, static function ($payment) {
        return (int) $payment->payment_type === PaymentTypes::GOOD_HOUSE_KEEPING_DEPOSIT &&
            in_array((int) $payment->payment_status, [
                PaymentStatus::AUTHENTICATE,
                PaymentStatus::PAYMENT,
                PaymentStatus::AUTHORISE,
            ]);
    });

    foreach ($booking->payments as $key => $payment) {
        if ((int) $payment->payment_status !== PaymentStatus::REFUNDED) {
            continue;
        }

        foreach ($ghbs as $ghb) {
            if ($ghb->payment_date < $payment->payment_date && $ghb->amount__number === $payment->amount__number) {
                if (! in_array($key, $offsets)) {
                    $offsets[] = $key;
                }
            }
        }
    }

    return $offsets;
}

function getSecurityDepositPaidAmount($booking)
{
    $sdPaidAmount = 0;

    foreach ($booking->payments as $payment) {
        if ((int) $payment->payment_type === PaymentTypes::GOOD_HOUSE_KEEPING_DEPOSIT &&
            in_array((int) $payment->payment_status, [
                PaymentStatus::AUTHENTICATE,
                PaymentStatus::PAYMENT,
                PaymentStatus::AUTHORISE,
                PaymentStatus::CANCEL,
                PaymentStatus::DESTROYED,
            ])) {
            $sdPaidAmount += $payment->amount__number;
        }
    }

    return $sdPaidAmount;
}

function getPropertyCommissionPercentage($booking): float
{
    return round($booking->commission_rate, 2);
}

function getBookingCommission($booking): float
{
    $commission = $booking->adjusted_rent * ($booking->commission_rate);
    $commission -= $booking->discount * ($booking->commission_rate);
    $commission -= $booking->special_offer * ($booking->commission_rate);

    return $commission;
}

function filterBooking($booking, $filter): bool
{
    return $filter;
}


function getDiscountType($booking): string
{
    if ($booking->discount > 0) {
        if (array_stripos((string) $booking->discount_description, [
                'owner discount',
                'owner discounts',
                'owners discount',
            ]
        )
        ) {
            return 'Owner Discount';
        }
        if (array_stripos((string) $booking->discount_description, [
                'lhh discount',
                'lhh discounts',
                'lhhs discount',
            ]
        )
        ) {
            return 'LHH Discount';
        }

        return 'Split Discount';
    }

    return '';
}

function getDiscountShare($booking): array
{
    $object = [
        'owner' => 0,
        'lhh'   => 0,
    ];

    if ($booking->discount > 0) {
        $discountType = getDiscountType($booking);

        if ($discountType === 'Owner Discount') {
            $object['owner'] = $booking->discount;
        }

        if ($discountType === 'LHH Discount') {
            $object['lhh'] = $booking->discount;
        }

        if ($discountType === 'Split Discount') {
            $lhhShare        = $booking->discount * $booking->commission_rate;
            $object['owner'] = $booking->discount - $lhhShare;
            $object['lhh']   = $lhhShare;
        }
    }

    return $object;
}

