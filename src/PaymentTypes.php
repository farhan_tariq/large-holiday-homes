<?php

final class PaymentTypes
{
    public const BALANCE_PAYMENT            = 0;
    public const FULL_PAYMENT               = 1;
    public const GOOD_HOUSE_KEEPING_DEPOSIT = 2;
    public const ADDITIONAL_DEPOSIT_PAYMENT = 3;
    public const OUTSTANDING_DEPOSIT        = 4;
    public const DEPOSIT_PAYMENT            = 5;
    public const REFUND_PAYMENT             = 6;
    public const PART_BALANCE_PAYMENT       = 7;
}
