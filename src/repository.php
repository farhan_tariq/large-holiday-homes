<?php

function getAllBookings(PDO $conn): array
{
    $bookings = [];
    $sql      = "SELECT
              bd.id as id,
              pa.`entity_id` AS _fk_property,
              bd.`customer` AS _fk_customer,
              DATE(bd.`booking_date`) AS booked_date,
              bd.`start_date` as from_date,
              bd.`end_date` as to_date,
              CASE
                   WHEN bd.`status` = -1 THEN 'Cancelled'
                   WHEN bd.`status` = 0  THEN 'Unsubmitted'
                   WHEN bd.`status` = 1  THEN 'In Progress'
                   WHEN bd.`status` = 3  THEN 'Confirmed'
                   WHEN bd.`status` = 4  THEN 'Paid In Full'
                   WHEN bd.`status` = 5  THEN 'Confirmed'
                   ELSE 'Owner booking'
                   END AS status,
              COALESCE(ROUND(bd.`value__number` + bd.`value_additional__number` + bd.`value_pet__number` + bd.`value_bed__number` + bd.`value_additional_cottage__number` + bd.`value_booking_fee__number` - bd.`discount_value__number` - bd.`special_offer_value__number`, 2), 0) AS total_price,
              COALESCE(ROUND(bd.`value__number`, 2), 0) AS actual_rent,
              COALESCE(ROUND(bd.`value__number`, 2), 0) + COALESCE(ROUND(bd.`value_additional__number`, 2), 0) AS adjusted_rent,
              COALESCE(ROUND(bd.`value_pet__number`, 2), 0) AS pet_cost,
              COALESCE(ROUND(bd.`value_bed__number`, 2), 0) AS bed_cost,
              COALESCE(ROUND(bd.`value_additional_cottage__number`, 2), 0) AS additional_cottage_cost,
              COALESCE(ROUND(bd.`value_booking_fee__number`, 2), 0) AS booking_fee,
              COALESCE(ROUND(bd.`value_additional_admin__number`, 2), 0) AS admin_fee,
              COALESCE(ROUND(bd.`discount_value__number`, 2), 0) AS discount,
              COALESCE(bd.discount_description, '') AS discount_description,
              COALESCE(ROUND(bd.`special_offer_value__number`, 2), 0) AS special_offer,
              COALESCE(ROUND(bd.`ghk_deposit__number`, 2), 0) AS security_deposit_amount,
              COALESCE(ROUND(bd.`ghk_deposit_paid__number`, 2), 0) AS security_deposit_paid_amount,
              COALESCE(ROUND(bd.`ghk_deposit_surcharge__number`, 2), 0) AS security_deposit_surcharge_amount,  
              bd.canceled,
              bd.booking_created_by,
              cd.field_commission_details_value / 100 as commission_rate,
              pn.`field_property_name_value` AS property_name,       
              od.`id` AS _fk_owner,
              bd.email_pending,
              bd.ready_for_accounts,
              bd.needs_confirmation,    
              CASE 
                 WHEN bd.required_action = 0 THEN 'Requires Initial Payment' 
                 WHEN bd.required_action = 1 THEN '1 - Empty'
                 WHEN bd.required_action = 2 THEN 'New Booking'
                 WHEN bd.required_action = 3 THEN 'Balance Overdue'
                 WHEN bd.required_action = 4 THEN 'GHD Overdue'
                 WHEN bd.required_action = 5 THEN 'Refund GHD' 
                 ELSE 'Empty'
                 END AS required_action
            FROM
              lhh_booking AS b
              JOIN lhh_booking_data bd
                ON bd.`vid` = b.`vid`
              LEFT JOIN node__field_property_address AS pa
                ON pa.`entity_id` = bd.`accommodation`
              LEFT JOIN `node__field_property_name` AS `pn`
                ON `pn`.`entity_id` = pa.`entity_id`
              LEFT JOIN `node__field_property_status` AS `ps`
                ON `ps`.`entity_id` = `pa`.`entity_id`
              LEFT JOIN `node__field_howner` AS `o`
                ON `o`.entity_id = `pa`.`entity_id`
              LEFT JOIN `lhh_owner_data` AS `od`
                ON `od`.id = o.field_howner_target_id
              JOIN node__field_commission_details cd 
                ON cd.entity_id = pa.entity_id
            WHERE (DATE(bd.booking_date) >= '2019-01-01' AND DATE(bd.booking_date) <= '" . BOOKINGS_END_DATE . "') OR 
                  (DATE(bd.booking_date) <= '" . BOOKINGS_END_DATE . "' AND YEAR(bd.start_date) > 2021)";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            $obj->rental_price                 = $obj->adjusted_rent - $obj->discount - $obj->special_offer;
            $obj->total_received_from_customer = 0;
            $obj->total_refunded_to_customer   = 0;
            $obj->security_deposit_refunded    = 0;
            $obj->payments                     = [];
            $obj->extras                       = [];
            $obj->owner_payments               = [];
            $obj->owner_payment_adjustments    = [];
            $bookings[$obj->id]                = $obj;
        }
    }

    echo "Fetched All Bookings" . PHP_EOL;
    return $bookings;
}

function getCustomerPayments(PDO $conn, array $bookings): array
{
    $payments = [];
    $sql      = "SELECT
              p.`id`,
              p.`booking`,
              p.`amount__number`,
              p.`payment_method`,
              p.`payment_date`,
              COALESCE(p.`payment_status`, 0) AS payment_status,
              COALESCE(p.`payment_type`, 0) AS payment_type
            FROM 
              lhh_booking b
              JOIN lhh_booking_data bd ON bd.`vid` = b.`vid`
              JOIN lhh_payment p       ON p.`booking` = b.`id`
            WHERE (
                (DATE(bd.`booking_date`) >= '2019-01-01' AND DATE(bd.booking_date) <= '" . BOOKINGS_END_DATE . "') OR 
                (DATE(bd.booking_date) <= '" . BOOKINGS_END_DATE . "' AND YEAR(bd.start_date) > 2021)
              )
              AND DATE(p.`payment_date`) <= '" . BOOKINGS_END_DATE . "'";

    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetchObject()) {
            if (! empty($bookings[$obj->booking])) {
                if (! in_array($obj->payment_status, [PaymentStatus::REFUNDED, PaymentStatus::VOID, PaymentStatus::HELD, PaymentStatus::CANCEL, PaymentStatus::DESTROYED])) {
                    $bookings[$obj->booking]->total_received_from_customer += $obj->amount__number;
                }

                if ($obj->payment_status == PaymentStatus::REFUNDED) {
                    $bookings[$obj->booking]->total_refunded_to_customer += $obj->amount__number;
                }

                $bookings[$obj->booking]->payments[] = $obj;
            }

            $payments[$obj->booking][] = $obj;
        }
    }

    echo "Fetched All Payments" . PHP_EOL;
    return $payments;
}