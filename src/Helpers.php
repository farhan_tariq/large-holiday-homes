<?php
include_once 'dotenv.php';
(new DotEnv(__DIR__ . '/../.env'))->load();

function array_stripos($haystack, $needles): bool
{
    $needles = (array) $needles;
    foreach ($needles as $needle) {
        if (stripos($haystack, $needle) !== false) {
            return true;
        }
    }

    return false;
}

function debugBooking(object $booking, int $bookingId, array $fields, bool $die = false): void
{
    if ((int) $booking->__pk === $bookingId) {
        print_r($fields);

        if ($die) {
            die('Finished debugBooking()');
        }
    }
}

function initDB()
{
    $host     = getenv('DB_SERVERNAME');
    $username = getenv('DB_USERNAME');
    $password = getenv('DB_PASSWORD');
    $database = getenv('DB_NAME');

    $dsn = 'mysql:host=' . $host . ';' . 'dbname=' . $database;

    // Create connection
    try {
        $conn = new PDO($dsn, $username, $password);
        echo 'Connected to ' . $host . ' ' . $database . ' Database !!' . PHP_EOL;
    } catch (PDOException $e) {
        die('Couldn\'t connect : ' . $e->getMessage());
    }

    return $conn;
}

function openFile($directory, $filename, $mode)
{
    if (! file_exists($directory)) {
        mkdir($directory, 0777, true);
    }

    $out = fopen($directory . $filename, $mode);

    if (! $out) {
        die('Could not open file: "' . $filename . '"');
    }

    return $out;
}

function addDescriptions($out, $headers, $counter)
{
    $descriptions = [];
    for ($i = 0; $i < $counter; $i++) {
        $descriptions[] = '';
    }

    fputcsv($out, array_merge($descriptions, $headers));
}

function addHeaders($out, $reportHeaders, $checkHeaders)
{
    fputcsv($out, array_merge($reportHeaders, $checkHeaders));
}
