<?php

include_once 'Helpers.php';
include_once 'FinanceChecks.php';
include_once 'Constants.php';
include_once 'PaymentStatus.php';
include_once 'PaymentTypes.php';

$conn          = initDB();
$financeChecks = new FinanceChecks();

$outputFileName = FILTER_BOOKINGS ? 'filtered-booking-' . DUMP_DATE . '-Dump-' . strtolower(BRAND_NAME) . '.csv' : 'filtered-booking-' . DUMP_DATE . '-Dump-' . strtolower(BRAND_NAME) . '.csv';

if (empty($argv[1])) {
    $out = openFile(EXPORT_DIRECTORY . '/' . DUMP_DATE . '/', $outputFileName, 'wb');
    addDescriptions($out, FINANCE_CHECK_HEADER_DESCRIPTIONS, 58);
    addHeaders($out, HEADERS_FILTERED_BOOKINGS, FINANCE_CHECK_HEADERS);
}

$knownFile = openFile(IMPORT_DIRECTORY . '/', KNOWN_BOOKINGS_FILE, 'rb');
$headers   = fgetcsv($knownFile);
$known     = [];

while (($row = fgetcsv($knownFile)) !== false) {
    $row               = array_combine($headers, $row);
    $known[$row['id']] = $row;
}